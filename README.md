# Installation Instructions

Go here to grab the Fabric Installer: https://fabricmc.net/use/

### If you are planning on hosting, read here
- Execute the installer and install as **server**. Make sure you use version **1.16.5** as that is the version that these mods support.
- This will begin downloading the server install for the version you selected. You'll be asked to provide from Java arguments when running it, but since you'll be hosting this server on linux anyawy, it would be best to use a script.

### If you are planning on playing on the server, read here
- Execute the installer and install as **client**. Make sure you use version **1.16.5** as that is the version that these mods support.


I was lazy and just used MultiMC which did it all for me lol o7


# Game Versions

Since this little repository was mainly for me playing around with... there are multiple versions of based on what branch you're on.

### Master branch
This version is pulling out some elements from the adventurepack and attempting to make it fit the boiler plate version that I had initially. Some parts of the `adventurepack`. Expect some glitches maybe? So far it seems pretty good. Might have to adjust some UI elements though. 


### adventurepackmods branch
This was a modpack that I found that makes the game diablo-like, darksouls-like, etc. Basically the core of the modpack is immersion and adventuring. I actually never fully tested out this modpack as a standalone test, but it's an _endorsed_ modpack so I assume it should be plug'n'play.


### only-qol-mods-for-solo branch
Consider this branch kind of like a `boiler plate` for a modded server. It can be played as-is, or you can add in your own themed mods and whatnot. It contains QoL things like JEI, World Map, and some QoL functionality additions like expanded storage and furnace options. There are other things like better world generation or better biomes, but they either use default textures or don't stray too far from base Minecraft look.


# How to make your game pretty

- Resource packs should be in the following order:

![order](https://i.imgur.com/4DzRAWF.png)

- There are currently 2 shader options you can enable if you'd like.
  - BSL Shader by default is pretty lightweight. Noticeable things include: strong lightshafts, strong bloom, some motion blur. The bloom makes everything look really bright.
  - SEUS shader has kind of been the go-to shaders for super realism. It definitely looks more "real". Less blinding. No motion blur. It is definitely more resource intensive than BSL shaders though. Don't forget to set texture resolution to 16 bit if using Mizunos!
