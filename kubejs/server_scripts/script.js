// priority: 0

settings.logAddedRecipes = true
settings.logRemovedRecipes = true
settings.logSkippedRecipes = false
settings.logErroringRecipes = true

console.info('Hello, World! (You will see this line every time server resources reload)')

onEvent('recipes', event => {
	event.remove({ id: "kibe:light_ring" })
	event.remove({ id: "kibe:water_ring" })
	event.remove({ id: "kibe:magma_ring" })
	event.remove({ id: "kibe:angel_ring" })
	event.remove({ id: "kibe:diamond_ring" })

	// Adding recipes to convert leather to respective rabbit pieces because rabbits are bugged
	event.shaped("8x minecraft:rabbit_hide", [
		"SS ",
		"   ",
		"   "
	], {
		S: "minecraft:leather"
	})

	event.shaped("1x minecraft:rabbit_foot", [
		"SSS",
		"SSS",
		"   "
	], {
		S: "minecraft:rabbit_hide"
	})
})

onEvent('item.tags', event => {
	// Get the #forge:cobblestone tag collection and add Diamond Ore to it
	// event.get('forge:cobblestone').add('minecraft:diamond_ore')

	// Get the #forge:cobblestone tag collection and remove Mossy Cobblestone from it
	// event.get('forge:cobblestone').remove('minecraft:mossy_cobblestone')
})